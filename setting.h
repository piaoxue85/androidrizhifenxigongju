#ifndef SETTING_H
#define SETTING_H

#include <QDialog>
#include <QDebug>
#include "mainwindow.h"

namespace Ui {
class Setting;
}

class Setting : public QDialog
{
    Q_OBJECT

public:
    explicit Setting(QWidget *parent = 0);
    ~Setting();

public:
    bool getIsClickTrue();  //退出时是否点击了确认
    QString getFontType();  //获取字体类型
    int getFontSize();      //获取字体大小
    QString getCodeType();  //获取编码类型
    QString getADBPath();   //获取ADB工具的路径
    bool getIsApplyOfflineMode();   //获取是否应用于离线模式
    bool getIsApplyRealtimeMode();  //获取是否应用于实时模式
    bool getIsFirstFilter();//获取是否启用一级过滤
    bool getIsSecondFilter();//获取是否启用二级过滤
    QStringList getFirstSysFilters();   //获取一级系统过滤表
    QStringList getSecondSysFilters();  //获取二级系统过滤表
    bool getIsApplyDebugApp();      //获取是否启用调试指定APP功能
    QString getCurDebugApp();       //获取当前调试APP的包名
    void transParams(QString fontType,int fontSize,
                     QString codeType,QString ADBPath,
                     bool isApplyOfflineMode,bool isApplyRealtimeMode,
                     bool isFirstFilter,bool isSecondFilter,
                     bool isApplyDebugApp,QString curDebugApp);   //向该对话框传入参数

private:
    void updatePreviewFont();       //更新预览字体样式

private slots:
    void btnOKClickedSlot();        //确认按钮槽函数
    void fontTypeChangedSlot();     //字体类型选择框内容变化槽函数
    void fontSizeChangedSlot();     //字体大小选择框内容变化槽函数
    void resetSlot();               //重置按钮槽函数
    void codeTypeChangedSlot();     //编码类型选择框内容变化槽函数
    void selectPathSlot();          //选择ADB工具路径的槽函数
    void isAppDebugAppChangeSlot(); //是否启用调试指定APP功能状态发生变化槽函数

private:
    Ui::Setting *ui;

    bool isClickTrue;   //是否点击了确认键
    QString fontType;   //字体类型
    int fontSize;       //字体大小
    QString codeType;   //编码类型
    QString ADBPath;    //ADB工具路径
    bool isApplyOfflineMode;    //是否应用于离线模式
    bool isApplyRealtimeMode;   //是否应用于实时模式
    bool isFirstFilter; //是否启用一级过滤
    bool isSecondFilter;//是否启用二级过滤
    QStringList firstSysFilters;//一级系统过滤表
    QStringList secondSysFilters;//二级系统过滤表
    bool isApplyDebugApp;//是否启用调试指定APP功能
    QString curDebugApp; //当前调试APP的包名
};

#endif // SETTING_H
